<?php
namespace Email;

use Email\DTO\VariablesCollection;
use Email\ValueObject\Variables\DeclineReasonVariablesCollectionFactory;
use Email\ValueObject\Variables\RfpVariablesCollectionFactory;
use Email\ValueObject\Variables\RoomVariablesCollectionFactory;
use Email\ValueObject\Variables\SuggestedRoomsVariablesCollectionFactory;
use Email\ValueObject\Variables\UserVariablesCollectionFactory;
use Email\ValueObject\Variables\RoomTagVariablesCollectionFactory;

class VariablesBuilder implements VariablesBuilderInterface
{
    /** @var  VariablesCollection */
    private $collection;

    /** @var  SuggestedRoomsVariablesCollectionFactory */
    private $suggestedRoomsVarFactory;

    /** @var  RfpVariablesCollectionFactory */
    private $rfpVariablesFactory;

    /** @var  DeclineReasonVariablesCollectionFactory */
    private $declineReasonsVariablesFactory;

    /** @var  RoomVariablesCollectionFactory */
    private $roomVariablesFactory;

    /** @var  UserVariablesCollectionFactory */
    private $userVariablesCollectionFactory;

    /** @var  RoomTagVariablesCollectionFactory */
    private $roomTagVariablesCollectionFactory;

    /**
     * @param SuggestedRoomsVariablesCollectionFactory $suggestedRoomsVarFactory
     * @param RfpVariablesCollectionFactory $rfpVariablesFactory
     * @param DeclineReasonVariablesCollectionFactory $declineReasonsVariablesFactory
     * @param RoomVariablesCollectionFactory $roomVariablesFactory
     * @param UserVariablesCollectionFactory $userVariablesCollectionFactory
     * @param roomTagVariablesCollectionFactory $roomTagVariablesCollectionFactory
     */
    public function __construct(SuggestedRoomsVariablesCollectionFactory $suggestedRoomsVarFactory, RfpVariablesCollectionFactory $rfpVariablesFactory, DeclineReasonVariablesCollectionFactory $declineReasonsVariablesFactory, RoomVariablesCollectionFactory $roomVariablesFactory, UserVariablesCollectionFactory $userVariablesCollectionFactory, RoomTagVariablesCollectionFactory $roomTagVariablesCollectionFactory)
    {
        $this->suggestedRoomsVarFactory = $suggestedRoomsVarFactory;
        $this->rfpVariablesFactory = $rfpVariablesFactory;
        $this->declineReasonsVariablesFactory = $declineReasonsVariablesFactory;
        $this->roomVariablesFactory = $roomVariablesFactory;
        $this->userVariablesCollectionFactory = $userVariablesCollectionFactory;
        $this->roomTagVariablesCollectionFactory = $roomTagVariablesCollectionFactory;
    }

    public function createCollection()
    {
        $this->collection = new VariablesCollection();
    }

    public function addSuggestedRoomVariables($rfpId)
    {
        $variables = $this->suggestedRoomsVarFactory->create($rfpId)->getGlobalVariables();
        $this->addVariables($variables);
    }

    public function addSuggestedRoomVariablesByEnquiry($enquiryId)
    {
        $variables = $this->suggestedRoomsVarFactory->createFromEnquiry($enquiryId)->getGlobalVariables();
        $this->addVariables($variables);
    }

    /**
     * @param $variables
     */
    private function addVariables($variables)
    {
        foreach ($variables as $variable) {
            $this->collection->push($variable);
        }
    }

    public function addRfpVariables($rfpId)
    {
        $variables = $this->rfpVariablesFactory->create($rfpId)->getGlobalVariables();
        $this->addVariables($variables);
    }

    public function addDeclineReasonVariables($rfpId)
    {
        $variables = $this->declineReasonsVariablesFactory->create($rfpId)->getGlobalVariables();
        $this->addVariables($variables);
    }

    public function addRoomVariables($rfpId)
    {
        $variables = $this->roomVariablesFactory->create($rfpId)->getGlobalVariables();
        $this->addVariables($variables);
    }

    public function addUserVariables($enquiryId)
    {
        $variables = $this->userVariablesCollectionFactory->create($enquiryId)->getGlobalVariables();
        $this->addVariables($variables);
    }

    public function addRoomTagVariables($enquiryId)
    {
        $variables = $this->roomTagVariablesCollectionFactory->create($enquiryId)->getGlobalVariables();
        $this->addVariables($variables);
    }

    /**
     * @return VariablesCollection
     */
    public function getVariables()
    {
        return $this->collection;
    }

}