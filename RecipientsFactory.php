<?php

namespace Email;

use Enquiries\Entity\EnquiryRfpEntity;
use Enquiries\Repository\EnquiryRfpsRepository;
use Enquiries\Repository\EnquiriesRepository;

class RecipientsFactory
{
    /** @var  EnquiryRfpsRepository */
    private $enquiryRfpsRepository;

    /** @var  EnquiriesRepository */
    private $enquiriesRepository;

    /**
     * @param EnquiryRfpsRepository $enquiryRfpsRepository
     * @param EnquiriesRepository $enquiriesRepository
     */
    public function __construct(EnquiryRfpsRepository $enquiryRfpsRepository, EnquiriesRepository $enquiriesRepository)
    {
        $this->enquiryRfpsRepository = $enquiryRfpsRepository;
        $this->enquiriesRepository = $enquiriesRepository;
    }
    /**
     * @param $rfpId
     * @return \Users\Entity\UserEntity
     */
    public function createRfpClient($rfpId)
    {
        /** @var EnquiryRfpEntity $rfp */  
        $rfp = $this->enquiryRfpsRepository->findWithAllData($rfpId);
        $client = $rfp->getEnquiry()->getClient();

        return $client;
    }

    /**
     * @param $enquiryId
     * @return \Users\Entity\UserEntity
     */
    public function createEnquiryClient($enquiryId)
    {
        /** @var EnquiryEntity $enquiry */  
        $enquiry = $this->enquiriesRepository->find($enquiryId);
        $client = $enquiry->getClient();

        return $client;
    }
}