<?php
namespace Email;

use Email\DTO\VariablesCollection;

interface VariablesBuilderInterface
{
    /**
     * @return VariablesCollection
     */
    public function getVariables();
}