<?php
namespace Email;

use DeSmart\Mailer\Variable;
use Email\Exception\EmailVariableNotFoundException;

trait FindVariableTrait
{
    /**
     * @param $variableName
     * @return Variable
     * @throws EmailVariableNotFoundException
     */
    public function findVariable($variableName)
    {
        $variables = $this->getVariables()->filter(function (Variable $variable) use ($variableName) {
            return $variable->getName() == $variableName;
        });

        /** @var Variable $variable */
        $variable = $variables->first();

        if($variable == null) {
            throw new EmailVariableNotFoundException();
        }
        return $variable;
    }
}