<?php
namespace Email\ValueObject\Sender;

interface SenderInterface
{
    /**
     * @return string
     */
    public function getName();

    /**
     * @return string
     */
    public function getEmail();
}