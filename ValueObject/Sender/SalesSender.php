<?php
namespace Email\ValueObject\Sender;

use DMS\SettingsManager;
use Email\MailAddress;

class SalesSender implements SenderInterface
{
    public function getName()
    {
        return SettingsManager::getStatic(MailAddress::SALES_NAME_IN_EMAIL);
    }

    public function getEmail()
    {
        return SettingsManager::getStatic(MailAddress::SALES_EMAIL);
    }
}