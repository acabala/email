<?php namespace Email\ValueObject\Variables;

use DeSmart\Mailer\Variable;
use Illuminate\Foundation\Bus\DispatchesCommands;
use Illuminate\Support\Collection;
use RoomTags\Entity\RoomTagEntity;

class RoomTagVariablesCollection
{
    use DispatchesCommands;

    /** @var  Collection */
    protected $globalVariables;

     /**
     * @param RoomTagEntity $roomTag
     */
    public function __construct(RoomTagEntity $roomTag)
    {
        $this->globalVariables = new Collection();
        $this->setGlobalVariables($roomTag);
    }

    private function setGlobalVariables(RoomTagEntity $roomTag)
    {
        $this->globalVariables->push(new Variable('room_tag', $roomTag->getName()));
    }

    public function getGlobalVariables()
    {
        return $this->globalVariables;
    }
}