<?php namespace Email\ValueObject\Variables;

use DeSmart\Mailer\Variable;
use Illuminate\Foundation\Bus\DispatchesCommands;
use Illuminate\Support\Collection;
use Users\Entity\UserEntity;

class UserVariablesCollection
{
    use DispatchesCommands;

    /** @var  Collection */
    protected $globalVariables;

    /**
     * @param UserEntity $user
     */
    public function __construct(UserEntity $user)
    {
        $this->globalVariables = new Collection();
        $this->setGlobalVariables($user);
    }

    private function setGlobalVariables(UserEntity $user)
    {
        $this->buildEnquiriesLink($user);
        $this->globalVariables->push(new Variable('client_name', $user->getFullname()));
    }

    /**
     * @param UserEntity $user
     */
    private function buildEnquiriesLink(UserEntity $user)
    {
        $route = route('client.enquiries.current');
        $autoLoginUrl = route('token-login', [
            'token' => $user->getEmailToken(),
            'user' => (string)$user->getId(),
            'base64url' => base64_encode($route)
        ]);
        $this->globalVariables->push(new Variable('link_to_enquiries', $autoLoginUrl));
    }

    public function getGlobalVariables()
    {
        return $this->globalVariables;
    }
}