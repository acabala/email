<?php
namespace Email\ValueObject\Variables;

use Enquiries\Entity\EnquiryEntity;
use Enquiries\Repository\EnquiriesRepository;
use Users\Entity\UserEntity;
use Users\Repository\UsersRepository;

class UserVariablesCollectionFactory
{
    /** @var  EnquiriesRepository */
    private $enquiriesRepository;

    /** @var  UsersRepository */
    private $usersRepository;

    /**
     * @param EnquiriesRepository $enquiriesRepository
     * @param UsersRepository $usersRepository
     */
    public function __construct(EnquiriesRepository $enquiriesRepository, UsersRepository $usersRepository)
    {
        $this->enquiriesRepository = $enquiriesRepository;
        $this->usersRepository = $usersRepository;
    }

    public function create($enquiryId)
    {
        /** @var EnquiryEntity $enquiry */
        $enquiry = $this->enquiriesRepository->find($enquiryId);
        /** @var UserEntity $user */
        $user = $this->usersRepository->find($enquiry->getClientId());

        $variables = new UserVariablesCollection($user);

        return $variables;
    }
}