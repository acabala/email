<?php
namespace Email\ValueObject\Variables;

use Enquiries\Entity\EnquiryEntity;
use Enquiries\Repository\EnquiriesRepository;
use RoomTags\Entity\RoomTagEntity;

class RoomTagVariablesCollectionFactory
{
    /** @var  EnquiriesRepository */
    private $enquiriesRepository;

    /**
     * @param EnquiriesRepository $enquiriesRepository
     */
    public function __construct(EnquiriesRepository $enquiriesRepository)
    {
        $this->enquiriesRepository = $enquiriesRepository;
    }

    public function create($enquiryId)
    {
        /** @var EnquiryEntity $enquiry */
        $enquiry = $this->enquiriesRepository->find($enquiryId);
        /** @var RoomTagEntity $roomTag */
        $roomTag = $enquiry->getRoomTag();

        $variables = new RoomTagVariablesCollection($roomTag);

        return $variables;
    }
}