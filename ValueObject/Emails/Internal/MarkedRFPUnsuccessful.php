<?php
namespace Email\ValueObject\Emails\Internal;

use DeSmart\Mailer\Mandrill\Mailer;
use DeSmart\Mailer\Recipient;
use Email\DTO\RecipientsCollection;
use Email\RecipientsFactory;
use Email\ValueObject\Emails\EmailBuilder;
use Email\ValueObject\Emails\EmailInterface;
use Email\ValueObject\Sender\SalesSender;
use Email\VariablesBuilder;
use Email\FindVariableTrait;
use Email\ValueObject\Sender\SenderInterface;

class MarkedRFPUnsuccessful implements EmailInterface
{
    use FindVariableTrait;

    const EMAIL_TEMPLATE = 'marked-rfp-unsuccessful';
    const EMAIL_SUBJECT = '%s marked RFP unsuccessful';
    /** @var  VariablesBuilder */
    protected $variablesBuilder;
    /** @var  EmailBuilder */
    protected $emailBuilder;
    /** @var  RecipientsFactory */
    protected $recipientsFactory;
    /** @var  SenderInterface */
    private $sender;
    /** @var  bool */
    private $recipientsPreserved = true;
    private $rfpId;

    public function __construct($rfpId, EmailBuilder $emailBuilder)
    {
        $this->setSender();
        $this->rfpId = $rfpId;
        $this->emailBuilder = $emailBuilder;
        $this->variablesBuilder = $emailBuilder->getVariablesBuilder();
        $this->recipientsFactory = $emailBuilder->getRecipientsFactory();
    }

    public function getTemplate()
    {
        return self::EMAIL_TEMPLATE;
    }

    public function getSubject()
    {
        return sprintf(self::EMAIL_SUBJECT, $this->findVariable('venue_name')->getValue());
    }

    public function getVariables()
    {
        $this->variablesBuilder->createCollection();
        $this->variablesBuilder->addDeclineReasonVariables($this->rfpId);
        $this->variablesBuilder->addRoomVariables($this->rfpId);

        return $this->variablesBuilder->getVariables();
    }

    public function getRecipients()
    {
        $recipients =  new RecipientsCollection();
        $recipients->push($this->getToRecipient());

        return $recipients;
    }

    private function getToRecipient()
    {
        $recipient = $this->getSender();
        return new Recipient($recipient->getName(), $recipient->getEmail());
    }

    public function getSender()
    {
        return $this->sender;
    }

    protected function setSender()
    {
        $this->sender = new SalesSender();
    }

    public function getMergeVarsLanguage()
    {
        return Mailer::MERGE_VARS_LANG_HANDLEBARS;
    }

    /**
     * @return boolean
     */
    public function isRecipientsPreserved()
    {
        return $this->recipientsPreserved;
    }
}