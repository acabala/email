<?php namespace Email\ValueObject\Emails;

use Email\DTO\RecipientsCollection;
use Email\DTO\VariablesCollection;
use Email\ValueObject\Sender\SenderInterface;

interface EmailInterface
{
    /**
     * @return string
     */
    public function getTemplate();

    /**
     * @return string
     */
    public function getSubject();

    /**
     * @return RecipientsCollection
     */
    public function getRecipients();

    /**
     * @return SenderInterface
     */
    public function getSender();

    /**
     * @return VariablesCollection
     */
    public function getVariables();

    /**
     * @return string
     */
    public function getMergeVarsLanguage();
    /**
     * @return bool
     */
    public function isRecipientsPreserved();
}