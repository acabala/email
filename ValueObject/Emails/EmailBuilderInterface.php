<?php
namespace Email\ValueObject\Emails;

use Email\RecipientsFactory;
use Email\VariablesBuilderInterface;

interface EmailBuilderInterface
{
    /**
     * @return RecipientsFactory
     */
    public function getRecipientsFactory();

    /**
     * @return VariablesBuilderInterface
     */
    public function getVariablesBuilder();
}