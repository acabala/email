<?php
namespace Email\ValueObject\Emails;

use Email\RecipientsFactory;
use Email\VariablesBuilder;

class EmailBuilder implements EmailBuilderInterface
{
    /** @var  VariablesBuilder */
    private $variablesBuilder;

    /** @var  RecipientsFactory */
    private $recipientsFactory;

    public function __construct(VariablesBuilder $variablesBuilder, RecipientsFactory $recipientsFactory)
    {
        $this->variablesBuilder = $variablesBuilder;
        $this->recipientsFactory = $recipientsFactory;
    }

    public function getRecipientsFactory()
    {
        return $this->recipientsFactory;
    }

    /**
     * @return VariablesBuilder
     */
    public function getVariablesBuilder()
    {
        return $this->variablesBuilder;
    }
}