<?php
namespace Email\ValueObject\Emails\Client;

use DeSmart\Mailer\Mandrill\Mailer;
use DeSmart\Mailer\Recipient;
use DeSmart\Mailer\RecipientType;
use Email\DTO\RecipientsCollection;
use Email\RecipientsFactory;
use Email\ValueObject\Emails\EmailBuilder;
use Email\ValueObject\Emails\EmailInterface;
use Email\ValueObject\Sender\SalesSender;
use Email\VariablesBuilder;
use Email\FindVariableTrait;
use Email\ValueObject\Sender\SenderInterface;

class Declined implements EmailInterface
{
    use FindVariableTrait;

    const EMAIL_TEMPLATE = 'declined-rfp-v1-1';
    const EMAIL_SUBJECT = '%s  isn\'t available, here are more options';
    /** @var  VariablesBuilder */
    protected $variablesBuilder;
    /** @var  EmailBuilder */
    protected $emailBuilder;
    /** @var  RecipientsFactory */
    protected $recipientsFactory;
    /** @var  SenderInterface */
    private $sender;
    /** @var  bool */
    private $recipientsPreserved = true;
    private $rfpId;

    public function __construct($rfpId, EmailBuilder $emailBuilder)
    {
        $this->setSender();
        $this->rfpId = $rfpId;
        $this->emailBuilder = $emailBuilder;
        $this->variablesBuilder = $emailBuilder->getVariablesBuilder();
        $this->recipientsFactory = $emailBuilder->getRecipientsFactory();
    }

    public function getTemplate()
    {
        return self::EMAIL_TEMPLATE;
    }

    public function getSubject()
    {
        return sprintf(self::EMAIL_SUBJECT, $this->findVariable('room_name')->getValue());
    }

    public function getVariables()
    {
        $this->variablesBuilder->createCollection();
        $this->variablesBuilder->addSuggestedRoomVariables($this->rfpId);
        $this->variablesBuilder->addRfpVariables($this->rfpId);
        $this->variablesBuilder->addDeclineReasonVariables($this->rfpId);
        $this->variablesBuilder->addRoomVariables($this->rfpId);

        return $this->variablesBuilder->getVariables();
    }

    public function getRecipients()
    {
        $recipients =  new RecipientsCollection();
        $recipients->push($this->getCCRecipient());
        $recipients->push($this->getToRecipient());

        return $recipients;
    }

    private function getCCRecipient()
    {
        return new Recipient($this->sender->getName(), $this->sender->getEmail(), RecipientType::cc());
    }

    private function getToRecipient()
    {
        $client = $this->recipientsFactory->createRfpClient($this->rfpId);
        return new Recipient($client->getFullname(), $client->getEmail());
    }

    public function getSender()
    {
        return $this->sender;
    }

    protected function setSender()
    {
        $this->sender = new SalesSender();
    }

    public function getMergeVarsLanguage()
    {
        return Mailer::MERGE_VARS_LANG_HANDLEBARS;
    }

    /**
     * @return boolean
     */
    public function isRecipientsPreserved()
    {
        return $this->recipientsPreserved;
    }
}