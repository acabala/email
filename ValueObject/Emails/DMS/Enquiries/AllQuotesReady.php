<?php
namespace Email\ValueObject\Emails\DMS\Enquiries;

use Enquiries\ValueObject\DmsManualSendingEmailTemplates;

class AllQuotesReady extends DmsEnquiriesEmail
{
    const EMAIL_SUBJECT = 'Your %s venue quotes';

    public function getTemplate()
    {
        return DmsManualSendingEmailTemplates::ALL_QUOTES_READY;
    }

    public function getSubject()
    {
        return sprintf(self::EMAIL_SUBJECT, $this->findVariable('room_tag')->getValue());
    }
}