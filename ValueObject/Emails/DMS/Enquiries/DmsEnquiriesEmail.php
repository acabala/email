<?php
namespace Email\ValueObject\Emails\DMS\Enquiries;

use Email\DTO\RecipientsCollection;
use Email\DTO\VariablesCollection;
use Email\ValueObject\Sender\SenderInterface;
use Email\ValueObject\Emails\EmailInterface;
use Email\ValueObject\Sender\SarahYoungSender;
use Email\ValueObject\Emails\EmailBuilder;
use Email\RecipientsFactory;
use Email\VariablesBuilder;
use DeSmart\Mailer\Mandrill\Mailer;
use DeSmart\Mailer\Recipient;
use Email\FindVariableTrait;

abstract class DmsEnquiriesEmail implements EmailInterface
{
    use FindVariableTrait;

    /** @var  VariablesBuilder */
    protected $variablesBuilder;
    /** @var  EmailBuilder */
    protected $emailBuilder;
    /** @var  RecipientsFactory */
    protected $recipientsFactory;
    /** @var  SenderInterface */
    protected $sender;
    /** @var  bool */
    protected $recipientsPreserved = true;
    /** @var integer*/
    protected $enquiryId;

    public function __construct($enquiryId, EmailBuilder $emailBuilder)
    {
        $this->setSender();
        $this->enquiryId = $enquiryId;
        $this->emailBuilder = $emailBuilder;
        $this->variablesBuilder = $emailBuilder->getVariablesBuilder();
        $this->recipientsFactory = $emailBuilder->getRecipientsFactory();
    }

    abstract public function getTemplate();

    abstract public function getSubject();
    /**
     * @return RecipientsCollection
     */
    public function getRecipients()
    {
        $recipients =  new RecipientsCollection();
        $recipients->push($this->getToRecipient());

        return $recipients;
    }
    public function getToRecipient()
    {
        $client = $this->recipientsFactory->createEnquiryClient($this->enquiryId);
        return new Recipient($client->getFullname(), $client->getEmail());
    }
    protected function setSender()
    {
        $this->sender = new SarahYoungSender();
    }
    /**
     * @return SenderInterface
     */
    public function getSender()
    {
        return $this->sender;
    }
    /**
     * @return VariablesCollection
     */
    public function getVariables()
    {
        $this->variablesBuilder->createCollection();
        $this->variablesBuilder->addSuggestedRoomVariablesByEnquiry($this->enquiryId);
        $this->variablesBuilder->addUserVariables($this->enquiryId);
        $this->variablesBuilder->addRoomTagVariables($this->enquiryId);

        return $this->variablesBuilder->getVariables();
    }
    public function getMergeVarsLanguage()
    {
        return Mailer::MERGE_VARS_LANG_HANDLEBARS;
    }
    /**
     * @return boolean
     */
    public function isRecipientsPreserved()
    {
        return $this->recipientsPreserved;
    }
}