<?php
namespace Email\UseCase;

use DeSmart\Mailer\MailerInterface;
use DeSmart\Mailer\Recipient;
use Email\DTO\RecipientsCollection;
use Email\DTO\VariablesCollection;
use Email\ValueObject\Emails\EmailInterface;
use Email\VariablesBuilder;

class SendInternalEmailCommandHandler
{
    /**
     * @var MailerInterface
     */
    private $mail;

    /** @var  VariablesBuilder */
    private $variablesBuilder;

    public function __construct(MailerInterface $mailInterface, VariablesBuilder $builder)
    {
        $this->mail = $mailInterface;
        $this->variablesBuilder = $builder;
    }

    /**
     * @param SendInternalEmailCommand $command
     */
    public function handle(SendInternalEmailCommand $command)
    {
        $email = $command->getEmail();

        $this->buildRecipients($email->getRecipients());
        $this->buildEmailParams($email);
        $this->mergeVariables($email->getVariables());

        $this->mail->send();
    }

    private function buildRecipients(RecipientsCollection $recipients)
    {
        /** @var Recipient $recipient */
        foreach ($recipients as $recipient) {
            $this->mail->addRecipient($recipient);
        }
    }

    private function buildEmailParams(EmailInterface $email)
    {
        $this->mail->setSubject($email->getSubject());
        $this->mail->setTemplate($email->getTemplate());

        $this->mail->setFromEmail($email->getSender()->getEmail());
        $this->mail->setFromName($email->getSender()->getName());
        $this->mail->setRecipientsPreserved($email->isRecipientsPreserved());
        $this->mail->setMergeVarsLanguage($email->getMergeVarsLanguage());
    }

    /**
     * @param VariablesCollection $collection
     */
    private function mergeVariables(VariablesCollection $collection)
    {
        foreach ($collection as $item) {
            $this->mail->addGlobalVariable($item);
        }
    }
}
