<?php
namespace Email\UseCase;

use Email\ValueObject\Emails\EmailInterface;

class SendInternalEmailCommand
{
    /**
     * @var EmailInterface
     */
    protected $email;

    /**
     * @param EmailInterface $email
     */
    public function __construct(EmailInterface $email)
    {
        $this->email = $email;
    }

    /**
     * @return EmailInterface
     */
    public function getEmail()
    {
        return $this->email;
    }
}
